## Spike Trap

> _"You were hit by the ground too hard."_

_Adds spike traps, false floors, and trap floors._

---

- [**Downloads**](https://www.curseforge.com/minecraft/mc-mods/thepeebrain-spike-trap/files)
- [**Wiki**](https://gitlab.com/thepeebrainmc/spike_trap/-/wikis/Home/)

**Required Mods**

- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api "Fabric API")
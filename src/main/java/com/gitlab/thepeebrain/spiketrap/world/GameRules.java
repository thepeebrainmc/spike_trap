package com.gitlab.thepeebrain.spiketrap.world;

import com.gitlab.thepeebrain.spiketrap.SpikeTrap;
import net.fabricmc.fabric.api.gamerule.v1.CustomGameRuleCategory;
import net.fabricmc.fabric.api.gamerule.v1.rule.DoubleRule;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.world.GameRules.BooleanRule;
import net.minecraft.world.GameRules.IntRule;
import net.minecraft.world.GameRules.Key;

import static net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory.createBooleanRule;
import static net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory.createDoubleRule;
import static net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory.createIntRule;
import static net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry.register;

public class GameRules
{
	public static final CustomGameRuleCategory SPIKE_TRAP_CATEGORY;

	public static final Key<DoubleRule> SPIKE_TRAP_BASE_DAMAGE;
	public static final Key<DoubleRule> SPIKE_TRAP_FALL_MULTIPLIER;
	public static final Key<DoubleRule> SPIKE_TRAP_WALK_MULTIPLIER;
	public static final Key<DoubleRule> SPIKE_TRAP_SNEAK_MULTIPLIER;

	public static final Key<BooleanRule> SPIKE_TRAP_DAMAGES_HOSTILE_MOBS;
	public static final Key<BooleanRule> SPIKE_TRAP_DAMAGES_PASSIVE_MOBS;
	public static final Key<BooleanRule> SPIKE_TRAP_DAMAGES_PLAYERS;
	public static final Key<BooleanRule> SPIKE_TRAP_DESTROYS_ITEMS;

	public static final Key<IntRule> FALSE_FLOOR_BREAK_DELAY;

	public static final Key<IntRule> TRAP_FLOOR_OPEN_DELAY;
	public static final Key<IntRule> TRAP_FLOOR_CLOSE_DELAY;

	public static void init() {}

	static
	{
		Identifier id = SpikeTrap.identifier("spike_trap_category");
		Text name = Text.translatable("key.categories.spike_trap").formatted(Formatting.BOLD, Formatting.YELLOW);
		SPIKE_TRAP_CATEGORY = new CustomGameRuleCategory(id, name);

		FALSE_FLOOR_BREAK_DELAY = register("falseFloorBreakDelay", SPIKE_TRAP_CATEGORY, createIntRule(2));

		SPIKE_TRAP_DAMAGES_HOSTILE_MOBS = register("spikeTrapDamagesHostileMobs", SPIKE_TRAP_CATEGORY,
				createBooleanRule(true));
		SPIKE_TRAP_DAMAGES_PASSIVE_MOBS = register("spikeTrapDamagesPassiveMobs", SPIKE_TRAP_CATEGORY,
				createBooleanRule(true));
		SPIKE_TRAP_DAMAGES_PLAYERS = register("spikeTrapDamagesPlayers", SPIKE_TRAP_CATEGORY,
				createBooleanRule(true));
		SPIKE_TRAP_DESTROYS_ITEMS = register("spikeTrapDestroysItems", SPIKE_TRAP_CATEGORY,
				createBooleanRule(false));

		SPIKE_TRAP_BASE_DAMAGE = register("spikeTrapBaseDamage", SPIKE_TRAP_CATEGORY,
				createDoubleRule(8.0f, 0.0f, 100.0f));

		SPIKE_TRAP_FALL_MULTIPLIER = register("spikeTrapMultiplierFall", SPIKE_TRAP_CATEGORY,
				createDoubleRule(1.0f, 0.0f, 100.0f));
		SPIKE_TRAP_SNEAK_MULTIPLIER = register("spikeTrapMultiplierSneak", SPIKE_TRAP_CATEGORY,
				createDoubleRule(0.0f, 0.0f, 100.0f));
		SPIKE_TRAP_WALK_MULTIPLIER = register("spikeTrapMultiplierWalk", SPIKE_TRAP_CATEGORY,
				createDoubleRule(0.25f, 0.0f, 100.0f));

		TRAP_FLOOR_CLOSE_DELAY = register("trapFloorDelayClose", SPIKE_TRAP_CATEGORY, createIntRule(100));
		TRAP_FLOOR_OPEN_DELAY = register("trapFloorDelayOpen", SPIKE_TRAP_CATEGORY, createIntRule(2));
	}
}

package com.gitlab.thepeebrain.spiketrap;

import com.gitlab.thepeebrain.spiketrap.block.Blocks;
import com.gitlab.thepeebrain.spiketrap.block.FalseFloorBlock;
import com.gitlab.thepeebrain.spiketrap.block.TrapFloorBlock;
import com.gitlab.thepeebrain.spiketrap.block.enums.TrapCover;
import com.gitlab.thepeebrain.spiketrap.item.Items;
import com.gitlab.thepeebrain.spiketrap.item.TrapCoverBlockItem;
import com.gitlab.thepeebrain.spiketrap.world.GameRules;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpikeTrap
		implements ModInitializer
{
	public static final String MOD_ID = "spike_trap";
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	public static final ItemGroup ITEM_GROUP;

	@Override
	public void onInitialize()
	{
		GameRules.init();
		registerItems();
		registerBlocks();
	}

	private void registerItems()
	{
		registerItem(Items.WOODEN_SPIKE, "wooden_spike");
		registerItem(Items.STONE_SPIKE, "stone_spike");
		registerItem(Items.IRON_SPIKE, "iron_spike");
		registerItem(Items.GOLDEN_SPIKE, "golden_spike");
		registerItem(Items.DIAMOND_SPIKE, "diamond_spike");
		registerItem(Items.NETHERITE_SPIKE, "netherite_spike");
	}

	private void registerBlocks()
	{
		registerBlock(Blocks.WOODEN_SPIKE_TRAP, "wooden_spike_trap");
		registerBlock(Blocks.STONE_SPIKE_TRAP, "stone_spike_trap");
		registerBlock(Blocks.IRON_SPIKE_TRAP, "iron_spike_trap");
		registerBlock(Blocks.GOLDEN_SPIKE_TRAP, "golden_spike_trap");
		registerBlock(Blocks.DIAMOND_SPIKE_TRAP, "diamond_spike_trap");
		registerBlock(Blocks.NETHERITE_SPIKE_TRAP, "netherite_spike_trap");

		registerBlockWithTypes(Blocks.FALSE_FLOOR, "false_floor", FalseFloorBlock.TRAP_COVER);
		registerBlockWithTypes(Blocks.TRAP_FLOOR, "trap_floor", TrapFloorBlock.TRAP_COVER);
	}

	private void registerBlock(Block block, String path)
	{
		Identifier id = identifier(path);
		Registry.register(Registries.BLOCK, id, block);
		registerItem(new BlockItem(block, new FabricItemSettings()), id);
	}

	private void registerBlockWithTypes(Block block, String path, EnumProperty<TrapCover> property)
	{
		Identifier id = identifier(path);
		Registry.register(Registries.BLOCK, id, block);

		for(TrapCover trapCover : property.getValues())
		{
			id = identifier(trapCover.getRegistryPrefix() + path);
			BlockItem blockItem = new TrapCoverBlockItem(block, trapCover, property, new FabricItemSettings());
			registerItem(blockItem, id);
		}
	}

	private void registerItem(Item item, String path)
	{
		registerItem(item, identifier(path));
	}

	private void registerItem(Item item, Identifier id)
	{
		Registry.register(Registries.ITEM, id, item);
		ItemGroupEvents.modifyEntriesEvent(SpikeTrap.ITEM_GROUP).register(content -> content.add(item));
	}

	public static Identifier identifier(String path)
	{
		return new Identifier(MOD_ID, path);
	}

	static
	{
		Identifier id = identifier("traps");
		ITEM_GROUP = FabricItemGroup.builder(id).icon(() -> new ItemStack(Items.IRON_SPIKE)).build();
	}
}

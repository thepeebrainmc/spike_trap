package com.gitlab.thepeebrain.spiketrap.block.enums;

import net.minecraft.block.Block;
import net.minecraft.registry.Registries;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.StringIdentifiable;

public enum TrapCover
		implements StringIdentifiable
{
	NONE("none"),
	CLAY("clay"),
	COARSE_DIRT("coarse_dirt"),
	CRIMSON_NYLIUM("crimson_nylium"),
	DIRT("dirt"),
	GRASS("grass"),
	GRAVEL("gravel"),
	MUD("mud"),
	MOSS("moss", "moss_block"),
	MYCELIUM("mycelium"),
	PODZOL("podzol"),
	RED_SAND("red_sand"),
	ROOTED_DIRT("rooted_dirt"),
	SAND("sand"),
	SNOW("snow", "snow_block"),
	SOUL_SAND("soul_sand"),
	SOUL_SOIL("soul_soil"),
	WARPED_NYLIUM("warped_nylium");

	private final String name;
	private final String baseBlockId;

	TrapCover(String name)
	{
		this(name, name);
	}

	TrapCover(String name, String baseBlockId)
	{
		this.name = name;
		this.baseBlockId = baseBlockId;
	}

	public BlockSoundGroup getSoundGroup()
	{
		Block block = Registries.BLOCK.get(new Identifier(baseBlockId));
		return block.getSoundGroup(block.getDefaultState());
	}

	public String getRegistryPrefix()
	{
		return this == NONE ? "" : name + "_";
	}

	@Override
	public String asString()
	{
		return this.name;
	}
}

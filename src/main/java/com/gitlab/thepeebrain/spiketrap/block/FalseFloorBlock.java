package com.gitlab.thepeebrain.spiketrap.block;

import com.gitlab.thepeebrain.spiketrap.block.enums.TrapCover;
import com.gitlab.thepeebrain.spiketrap.world.GameRules;
import net.minecraft.block.*;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundCategory;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.random.Random;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;

import static net.minecraft.block.Blocks.AIR;

public class FalseFloorBlock
		extends Block
{
	public static final EnumProperty<TrapCover> TRAP_COVER;
	public static final IntProperty DEPTH;

	protected static final VoxelShape SHAPE;

	// ============================================================================================================== //

	public FalseFloorBlock(AbstractBlock.Settings settings)
	{
		super(settings);
		this.setDefaultState(this.getDefaultState()
				.with(TRAP_COVER, TrapCover.DIRT)
				.with(DEPTH, 0));
	}

	// ============================================================================================================== //

	@Override
	public void onSteppedOn(World world, BlockPos pos, BlockState state, Entity entity)
	{
		super.onSteppedOn(world, pos, state, entity);

		if(!world.isClient && !entity.isSneaking())
		{
			tryToBreak((ServerWorld)world, pos, state, entity, false);
		}
	}

	@Override
	public void onLandedUpon(World world, BlockState state, BlockPos pos, Entity entity, float fallDistance)
	{
		super.onLandedUpon(world, state, pos, entity, fallDistance);

		if(!world.isClient)
		{
			tryToBreak((ServerWorld)world, pos, state, entity, true);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onProjectileHit(World world, BlockState state, BlockHitResult hit, ProjectileEntity projectile)
	{
		if(!world.isClient() && hit.getSide() == Direction.UP)
		{
			tryToBreak((ServerWorld)world, hit.getBlockPos(), state, projectile, true);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {}

	@SuppressWarnings("deprecation")
	@Override
	public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random)
	{
		for(Direction direction : Direction.Type.HORIZONTAL)
		{
			BlockPos offsetPos = pos.offset(direction);
			BlockState offsetState = world.getBlockState(offsetPos);
			if(offsetState.getBlock() instanceof FalseFloorBlock falseFloorBlock)
			{
				world.scheduleBlockTick(offsetPos, falseFloorBlock, 1);
			}
		}

		world.setBlockState(pos, AIR.getDefaultState(), Block.NOTIFY_ALL);
		world.playSound(null, pos, getSoundGroup(state).getBreakSound(), SoundCategory.BLOCKS);
	}

	private void tryToBreak(ServerWorld world, BlockPos pos, BlockState state, Entity entity, boolean fast)
	{
		if(entity instanceof PlayerEntity player)
		{
			ItemStack boots = player.getInventory().getArmorStack(0);
			if(EnchantmentHelper.getLevel(Enchantments.FEATHER_FALLING, boots) > state.get(DEPTH))
			{
				return;
			}
		}

		if(fast)
		{
			scheduledTick(state, world, pos, null);
		}
		else
		{
			world.scheduleBlockTick(pos, this, world.getGameRules().getInt(GameRules.FALSE_FLOOR_BREAK_DELAY));
		}
	}

	// ============================================================================================================== //

	@Override
	public BlockState getPlacementState(ItemPlacementContext ctx)
	{
		World world = ctx.getWorld();
		BlockPos pos = ctx.getBlockPos();
		int depth = 4;
		for(Direction direction : Direction.Type.HORIZONTAL)
		{
			BlockPos offsetPos = pos.offset(direction);
			BlockState offsetState = world.getBlockState(offsetPos);
			if(offsetState.isSideSolid(world, offsetPos, direction.getOpposite(), SideShapeType.FULL))
			{
				depth = 0;
				break;
			}
			else if(offsetState.getBlock() instanceof FalseFloorBlock)
			{
				depth = Math.min(depth, offsetState.get(DEPTH) + 1);
			}
		}

		return depth == 4 ? null : getDefaultState().with(DEPTH, depth);
	}

	@SuppressWarnings("deprecation")
	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState,
			WorldAccess world, BlockPos pos, BlockPos neighborPos)
	{
		if(direction.getAxis() == Direction.Axis.Y || neighborState.getBlock() instanceof FalseFloorBlock)
		{
			return state;
		}

		for(Direction d : Direction.Type.HORIZONTAL)
		{
			BlockPos offsetPos = pos.offset(d);
			BlockState offsetState = world.getBlockState(offsetPos);
			if((offsetState.getBlock() instanceof FalseFloorBlock && offsetState.get(DEPTH) < state.get(DEPTH))
					|| offsetState.isSideSolid(world, offsetPos, d.getOpposite(), SideShapeType.FULL))
			{
				return state;
			}
		}

		return AIR.getDefaultState();
	}

	// ============================================================================================================== //

	@SuppressWarnings("deprecation")
	@Override
	public boolean hasSidedTransparency(BlockState state)
	{
		return true;
	}

	@Override
	public BlockSoundGroup getSoundGroup(BlockState state)
	{
		return state.get(TRAP_COVER).getSoundGroup();
	}

	@SuppressWarnings("deprecation")
	@Override
	public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context)
	{
		return SHAPE;
	}

	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder)
	{
		builder.add(TRAP_COVER, DEPTH);
	}

	// ============================================================================================================== //

	static
	{
		TRAP_COVER = EnumProperty.of("cover", TrapCover.class, trapCover -> trapCover != TrapCover.NONE);
		DEPTH = IntProperty.of("depth", 0, 3);
		SHAPE = Block.createCuboidShape(0.0, 14.0, 0.0, 16.0, 16.0, 16.0);
	}
}

package com.gitlab.thepeebrain.spiketrap.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.MapColor;
import net.minecraft.block.Material;
import net.minecraft.item.ToolMaterials;
import net.minecraft.sound.BlockSoundGroup;

public class Blocks
{
	public static Block WOODEN_SPIKE_TRAP;
	public static Block STONE_SPIKE_TRAP;
	public static Block IRON_SPIKE_TRAP;
	public static Block GOLDEN_SPIKE_TRAP;
	public static Block DIAMOND_SPIKE_TRAP;
	public static Block NETHERITE_SPIKE_TRAP;

	public static Block FALSE_FLOOR;
	public static Block TRAP_FLOOR;

	static
	{
		AbstractBlock.Settings spikeTrapSettings = AbstractBlock.Settings
				.of(Material.STONE, MapColor.DEEPSLATE_GRAY)
				.sounds(BlockSoundGroup.POLISHED_DEEPSLATE)
				.strength(3.5F, 6.0F)
				.requiresTool();
		WOODEN_SPIKE_TRAP = new SpikeTrapBlock(spikeTrapSettings, ToolMaterials.WOOD);
		STONE_SPIKE_TRAP = new SpikeTrapBlock(spikeTrapSettings, ToolMaterials.STONE);
		IRON_SPIKE_TRAP = new SpikeTrapBlock(spikeTrapSettings, ToolMaterials.IRON);
		GOLDEN_SPIKE_TRAP = new SpikeTrapBlock(spikeTrapSettings, ToolMaterials.GOLD);
		DIAMOND_SPIKE_TRAP = new SpikeTrapBlock(spikeTrapSettings, ToolMaterials.DIAMOND);
		NETHERITE_SPIKE_TRAP = new SpikeTrapBlock(spikeTrapSettings, ToolMaterials.NETHERITE);

		AbstractBlock.Settings falseFloorSettings = AbstractBlock.Settings
				.of(Material.SOIL, MapColor.DIRT_BROWN)
				.strength(0.5F)
				.sounds(BlockSoundGroup.GRAVEL);
		FALSE_FLOOR = new FalseFloorBlock(falseFloorSettings);

		AbstractBlock.Settings trapFloorSettings = AbstractBlock.Settings
				.of(Material.METAL, MapColor.IRON_GRAY)
				.strength(5.0F, 6.0F)
				.sounds(BlockSoundGroup.METAL)
				.requiresTool();
		TRAP_FLOOR = new TrapFloorBlock(trapFloorSettings);
	}
}

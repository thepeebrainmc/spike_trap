package com.gitlab.thepeebrain.spiketrap.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.Waterloggable;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.ai.pathing.NavigationType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolMaterial;
import net.minecraft.item.ToolMaterials;
import net.minecraft.registry.tag.FluidTags;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;

import static com.gitlab.thepeebrain.spiketrap.world.GameRules.*;

public class SpikeTrapBlock
		extends Block
		implements Waterloggable
{
	public static final BooleanProperty WATERLOGGED;
	protected static final VoxelShape SHAPE;

	private final ToolMaterial toolMaterial;

	// ============================================================================================================== //

	public SpikeTrapBlock(Settings settings, ToolMaterial toolMaterial)
	{
		super(settings);
		this.toolMaterial = toolMaterial;
		this.setDefaultState(this.getDefaultState().with(WATERLOGGED, false));
	}

	// DAMAGING ===================================================================================================== //

	@Override
	public void onSteppedOn(World world, BlockPos pos, BlockState state, Entity entity)
	{
		super.onSteppedOn(world, pos, state, entity);

		GameRules rules = world.getGameRules();
		double multiplier = entity.isSneaking() ?
				rules.get(SPIKE_TRAP_SNEAK_MULTIPLIER).get() : rules.get(SPIKE_TRAP_WALK_MULTIPLIER).get();
		damageEntity(entity, rules, (float)multiplier, false);
	}

	@Override
	public void onLandedUpon(World world, BlockState state, BlockPos pos, Entity entity, float fallDistance)
	{
		super.onLandedUpon(world, state, pos, entity, fallDistance);

		GameRules rules = world.getGameRules();
		damageEntity(entity, rules, (float)rules.get(SPIKE_TRAP_FALL_MULTIPLIER).get(), true);
	}

	private void damageEntity(Entity entity, GameRules rules, float multiplier, boolean fell)
	{
		if(!rules.getBoolean(SPIKE_TRAP_DAMAGES_PLAYERS) && entity instanceof PlayerEntity)
		{
			return;
		}
		else if(!rules.getBoolean(SPIKE_TRAP_DESTROYS_ITEMS) && entity instanceof ItemEntity)
		{
			return;
		}
		else if(entity instanceof MobEntity)
		{
			if(entity instanceof PassiveEntity)
			{
				if(!rules.getBoolean(SPIKE_TRAP_DAMAGES_PASSIVE_MOBS))
				{
					return;
				}
			}
			else if(!rules.getBoolean(SPIKE_TRAP_DAMAGES_HOSTILE_MOBS))
			{
				return;
			}
		}

		String damageSource = "spikeTrap";
		double base = rules.get(SPIKE_TRAP_BASE_DAMAGE).get();
		float damage = ((float)base + toolMaterial.getAttackDamage()) * multiplier;
		if(entity instanceof PlayerEntity player)
		{
			ItemStack boots = player.getInventory().armor.get(0);
			if(!boots.isEmpty() && boots.hasEnchantments())
			{
				float level = EnchantmentHelper.getLevel(Enchantments.FEATHER_FALLING, boots);
				if(level > 0)
				{
					if(toolMaterial != ToolMaterials.GOLD)
					{
						damage *= 1.0f - Math.max(4, level) / 4.0f;
					}
					else
					{
						damageSource = fell ? "spikeTrapGoldFell" : "spikeTrapGoldStep";
					}
				}
			}
		}
		entity.damage(new DamageSource(damageSource), damage);
	}

	// PLACEMENT ==================================================================================================== //

	@Override
	public BlockState getPlacementState(ItemPlacementContext ctx)
	{
		BlockPos blockPos = ctx.getBlockPos();
		FluidState fluidState = ctx.getWorld().getFluidState(blockPos);
		return this.getDefaultState().with(WATERLOGGED, fluidState.getFluid() == Fluids.WATER);
	}

	// WATERLOGGING ================================================================================================= //

	@SuppressWarnings("deprecation")
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.get(WATERLOGGED) ? Fluids.WATER.getStill(false) : super.getFluidState(state);
	}

	@Override
	public boolean tryFillWithFluid(WorldAccess world, BlockPos pos, BlockState state, FluidState fluidState)
	{
		return Waterloggable.super.tryFillWithFluid(world, pos, state, fluidState);
	}

	@Override
	public boolean canFillWithFluid(BlockView world, BlockPos pos, BlockState state, Fluid fluid)
	{
		return Waterloggable.super.canFillWithFluid(world, pos, state, fluid);
	}

	@SuppressWarnings("deprecation")
	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState,
			WorldAccess world, BlockPos pos, BlockPos neighborPos)
	{
		if(state.get(WATERLOGGED))
		{
			world.scheduleFluidTick(pos, Fluids.WATER, Fluids.WATER.getTickRate(world));
		}

		return super.getStateForNeighborUpdate(state, direction, neighborState, world, pos, neighborPos);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean canPathfindThrough(BlockState state, BlockView world, BlockPos pos, NavigationType type)
	{
		return switch(type)
				{
					case LAND, AIR -> false;
					case WATER -> world.getFluidState(pos).isIn(FluidTags.WATER);
				};
	}

	// ============================================================================================================== //

	@SuppressWarnings("deprecation")
	@Override
	public boolean hasSidedTransparency(BlockState state)
	{
		return true;
	}

	@SuppressWarnings("deprecation")
	@Override
	public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context)
	{
		return SHAPE;
	}

	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder)
	{
		builder.add(WATERLOGGED);
	}

	// ============================================================================================================== //

	static
	{
		WATERLOGGED = Properties.WATERLOGGED;
		SHAPE = Block.createCuboidShape(0.0, 0.0, 0.0, 16.0, 8.0, 16.0);
	}
}

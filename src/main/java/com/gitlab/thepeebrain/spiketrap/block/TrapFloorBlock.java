package com.gitlab.thepeebrain.spiketrap.block;

import com.gitlab.thepeebrain.spiketrap.block.enums.TrapCover;
import com.gitlab.thepeebrain.spiketrap.world.GameRules;
import net.minecraft.block.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.random.Random;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;

import static net.minecraft.block.Blocks.AIR;

public class TrapFloorBlock
		extends HorizontalFacingBlock
{
	public static final BooleanProperty OPEN;
	public static final EnumProperty<TrapCover> TRAP_COVER;

	private static final VoxelShape TOP_SHAPE;
	private static final VoxelShape NORTH1_SHAPE;
	private static final VoxelShape NORTH2_SHAPE;
	private static final VoxelShape SOUTH1_SHAPE;
	private static final VoxelShape SOUTH2_SHAPE;
	private static final VoxelShape WEST1_SHAPE;
	private static final VoxelShape WEST2_SHAPE;
	private static final VoxelShape EAST1_SHAPE;
	private static final VoxelShape EAST2_SHAPE;

	// ============================================================================================================== //

	public TrapFloorBlock(AbstractBlock.Settings settings)
	{
		super(settings);
		this.setDefaultState(this.getDefaultState()
				.with(FACING, Direction.NORTH)
				.with(OPEN, false)
				.with(TRAP_COVER, TrapCover.NONE)
		);
	}

	// TRIGGERING =================================================================================================== //

	@Override
	public void onSteppedOn(World world, BlockPos pos, BlockState state, Entity entity)
	{
		super.onSteppedOn(world, pos, state, entity);

		if(!world.isClient && !entity.isSneaking())
		{
			tryToOpen((ServerWorld)world, pos, state);
		}
	}

	@Override
	public void onLandedUpon(World world, BlockState state, BlockPos pos, Entity entity, float fallDistance)
	{
		super.onLandedUpon(world, state, pos, entity, fallDistance);

		if(!world.isClient)
		{
			tryToOpen((ServerWorld)world, pos, state);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onProjectileHit(World world, BlockState state, BlockHitResult hit, ProjectileEntity projectile)
	{
		if(!world.isClient() && hit.getSide() == Direction.UP)
		{
			tryToOpen((ServerWorld)world, hit.getBlockPos(), state);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random)
	{
		if(state.get(OPEN))
		{
			world.setBlockState(pos, state.with(OPEN, false), Block.NOTIFY_ALL);
			world.playSound(null, pos, SoundEvents.BLOCK_IRON_TRAPDOOR_OPEN, SoundCategory.BLOCKS);
		}
		else
		{
			world.setBlockState(pos, state.with(OPEN, true), Block.NOTIFY_ALL);
			int delay = world.getGameRules().getInt(GameRules.TRAP_FLOOR_CLOSE_DELAY);
			world.scheduleBlockTick(pos, this, delay);
			world.playSound(null, pos, SoundEvents.BLOCK_IRON_TRAPDOOR_CLOSE, SoundCategory.BLOCKS);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {}

	private void tryToOpen(ServerWorld world, BlockPos pos, BlockState state)
	{
		if(state.get(OPEN))
		{
			return;
		}

		int delay = world.getGameRules().getInt(GameRules.TRAP_FLOOR_OPEN_DELAY);
		world.scheduleBlockTick(pos, this, delay);

		BlockPos offsetPos = pos.offset(state.get(FACING));
		BlockState offsetState = world.getBlockState(offsetPos);
		if(offsetState.getBlock() instanceof TrapFloorBlock && !offsetState.get(OPEN))
		{
			world.scheduleBlockTick(offsetPos, offsetState.getBlock(), delay);
		}
	}

	// PLACEMENT ==================================================================================================== //

	@SuppressWarnings("deprecation")
	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState,
			WorldAccess world, BlockPos pos, BlockPos neighborPos)
	{
		return direction.getOpposite() == state.get(FACING) &&
				!neighborState.isSideSolidFullSquare(world, neighborPos, direction.getOpposite()) ?
				AIR.getDefaultState() : state;
	}

	@Override
	public BlockState getPlacementState(ItemPlacementContext ctx)
	{
		Direction hitSide = ctx.getSide();

		World world = ctx.getWorld();
		BlockPos pos = ctx.getBlockPos();
		if(hitSide.getAxis() != Direction.Axis.Y && canMountToSide(world, pos, hitSide.getOpposite()))
		{
			return this.getDefaultState().with(FACING, hitSide);
		}

		for(Direction direction : Direction.Type.HORIZONTAL)
		{
			if(direction == hitSide.getOpposite())
			{
				continue;
			}
			if(canMountToSide(world, pos, direction))
			{
				return this.getDefaultState().with(FACING, direction.getOpposite());
			}
		}

		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean canPlaceAt(BlockState state, WorldView world, BlockPos pos)
	{
		for(Direction direction : Direction.Type.HORIZONTAL)
		{
			if(canMountToSide(world, pos, direction))
			{
				return true;
			}
		}
		return false;
	}

	private boolean canMountToSide(WorldView world, BlockPos pos, Direction direction)
	{
		BlockPos offsetPos = pos.offset(direction);
		BlockState offsetState = world.getBlockState(offsetPos);
		return offsetState.isSideSolidFullSquare(world, offsetPos, direction.getOpposite());
	}

	// COVERING ===================================================================================================== //

	@SuppressWarnings("deprecation")
	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hit)
	{
		boolean isOpen = state.get(OPEN);
		Direction side = hit.getSide();
		if(player.isSneaking()
				&& ((isOpen && side == state.get(TrapFloorBlock.FACING)) || !isOpen && side == Direction.UP))
		{
			TrapCover type = state.get(TRAP_COVER);

			if(type == TrapCover.NONE)
			{
				return ActionResult.PASS;
			}

			if(!world.isClient)
			{
				world.setBlockState(pos, state.with(TRAP_COVER, TrapCover.NONE), Block.NOTIFY_LISTENERS);
				world.playSound(null, pos, type.getSoundGroup().getBreakSound(), SoundCategory.BLOCKS);
			}

			return ActionResult.success(world.isClient);
		}
		else if(!isOpen && side != Direction.UP)
		{
			if(!world.isClient)
			{
				tryToOpen((ServerWorld)world, pos, state);
			}
			return ActionResult.success(world.isClient);
		}
		else
		{
			return ActionResult.PASS;
		}
	}

	// ============================================================================================================== //

	@SuppressWarnings("deprecation")
	@Override
	public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context)
	{
		if(state.get(OPEN))
		{
			switch(state.get(FACING))
			{
				case NORTH:
					return VoxelShapes.union(NORTH1_SHAPE, NORTH2_SHAPE);
				case EAST:
					return VoxelShapes.union(EAST1_SHAPE, EAST2_SHAPE);
				case SOUTH:
					return VoxelShapes.union(SOUTH1_SHAPE, SOUTH2_SHAPE);
				case WEST:
					return VoxelShapes.union(WEST1_SHAPE, WEST2_SHAPE);
			}
		}
		else
		{
			switch(state.get(FACING))
			{
				case NORTH:
					return VoxelShapes.union(NORTH1_SHAPE, TOP_SHAPE);
				case EAST:
					return VoxelShapes.union(EAST1_SHAPE, TOP_SHAPE);
				case SOUTH:
					return VoxelShapes.union(SOUTH1_SHAPE, TOP_SHAPE);
				case WEST:
					return VoxelShapes.union(WEST1_SHAPE, TOP_SHAPE);
			}
		}
		return TOP_SHAPE;
	}

	@SuppressWarnings("deprecation")
	@Override
	public VoxelShape getCollisionShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context)
	{
		return state.get(OPEN) ? VoxelShapes.empty() : state.getOutlineShape(world, pos);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean hasSidedTransparency(BlockState state)
	{
		return true;
	}

	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder)
	{
		builder.add(FACING, OPEN, TRAP_COVER);
	}

	// ============================================================================================================== //

	static
	{
		OPEN = BooleanProperty.of("open");
		TRAP_COVER = EnumProperty.of("cover", TrapCover.class);

		TOP_SHAPE = Block.createCuboidShape(0.0, 14.0, 0.0, 16.0, 16.0, 16.0);
		NORTH1_SHAPE = Block.createCuboidShape(5.0, 6.0, 14.0, 11.0, 14.0, 16.0);
		NORTH2_SHAPE = Block.createCuboidShape(0.0, 0.0, 12.0, 16.0, 16.0, 14.0);
		EAST1_SHAPE = Block.createCuboidShape(0.0, 6.0, 5.0, 2.0, 14.0, 11.0);
		EAST2_SHAPE = Block.createCuboidShape(2.0, 0.0, 0.0, 4.0, 16.0, 16.0);
		SOUTH1_SHAPE = Block.createCuboidShape(5.0, 6.0, 0.0, 11.0, 14.0, 2.0);
		SOUTH2_SHAPE = Block.createCuboidShape(0.0, 0.0, 2.0, 16.0, 16.0, 4.0);
		WEST1_SHAPE = Block.createCuboidShape(14.0, 6.0, 5.0, 16.0, 14.0, 11.0);
		WEST2_SHAPE = Block.createCuboidShape(12.0, 0.0, 0.0, 14.0, 16.0, 16.0);
	}
}

package com.gitlab.thepeebrain.spiketrap.item;

import net.minecraft.item.Item;

public class Items
{
	public static Item WOODEN_SPIKE;
	public static Item STONE_SPIKE;
	public static Item IRON_SPIKE;
	public static Item GOLDEN_SPIKE;
	public static Item DIAMOND_SPIKE;
	public static Item NETHERITE_SPIKE;

	static
	{
		WOODEN_SPIKE = new Item(new Item.Settings());
		STONE_SPIKE = new Item(new Item.Settings());
		IRON_SPIKE = new Item(new Item.Settings());
		GOLDEN_SPIKE = new Item(new Item.Settings());
		DIAMOND_SPIKE = new Item(new Item.Settings());
		NETHERITE_SPIKE = new Item(new Item.Settings());
	}
}

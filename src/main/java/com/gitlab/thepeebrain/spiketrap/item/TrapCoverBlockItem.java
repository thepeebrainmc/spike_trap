package com.gitlab.thepeebrain.spiketrap.item;

import com.gitlab.thepeebrain.spiketrap.SpikeTrap;
import com.gitlab.thepeebrain.spiketrap.block.FalseFloorBlock;
import com.gitlab.thepeebrain.spiketrap.block.TrapFloorBlock;
import com.gitlab.thepeebrain.spiketrap.block.enums.TrapCover;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.registry.Registries;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class TrapCoverBlockItem
		extends BlockItem
{
	private final TrapCover trapCover;
	private final EnumProperty<TrapCover> property;

	// ============================================================================================================== //

	public TrapCoverBlockItem(Block block, TrapCover trapCover, EnumProperty<TrapCover> property, Settings settings)
	{
		super(block, settings);
		this.trapCover = trapCover;
		this.property = property;
	}

	// ============================================================================================================== //

	@Override
	public ActionResult useOnBlock(ItemUsageContext context)
	{
		if(!(getBlock() instanceof FalseFloorBlock))
		{
			return super.useOnBlock(context);
		}

		PlayerEntity player = context.getPlayer();

		if(player != null && !player.isSneaking())
		{
			return super.useOnBlock(context);
		}

		World world = context.getWorld();
		BlockPos pos = context.getBlockPos();
		BlockState state = world.getBlockState(pos);

		if(!(state.getBlock() instanceof TrapFloorBlock))
		{
			return super.useOnBlock(context);
		}
		else
		{
			boolean isOpen = state.get(TrapFloorBlock.OPEN);
			Direction side = context.getSide();
			if(!((isOpen && side == state.get(TrapFloorBlock.FACING))
					|| !isOpen && side == Direction.UP))
			{
				return super.useOnBlock(context);
			}
		}

		if(state.get(TrapFloorBlock.TRAP_COVER) == trapCover)
		{
			return ActionResult.PASS;
		}

		if(!world.isClient)
		{
			world.setBlockState(pos, state.with(TrapFloorBlock.TRAP_COVER, trapCover), Block.NOTIFY_LISTENERS);
			world.playSound(null, pos, trapCover.getSoundGroup().getPlaceSound(), SoundCategory.BLOCKS);
		}

		if(player != null && !player.getAbilities().creativeMode)
		{
			context.getStack().decrement(1);
		}

		return ActionResult.success(world.isClient);
	}

	public String getTranslationKey()
	{
		String path = trapCover.getRegistryPrefix() + Registries.BLOCK.getId(getBlock()).getPath();
		Identifier id = SpikeTrap.identifier(path);
		return Util.createTranslationKey("block", id);
	}

	protected SoundEvent getPlaceSound(BlockState state)
	{
		return state.get(property).getSoundGroup().getPlaceSound();
	}

	@Nullable
	protected BlockState getPlacementState(ItemPlacementContext context)
	{
		BlockState blockState = this.getBlock().getPlacementState(context);
		if(blockState != null)
		{
			blockState = blockState.with(property, trapCover);
			return this.canPlace(context, blockState) ? blockState : null;
		}

		return null;
	}
}

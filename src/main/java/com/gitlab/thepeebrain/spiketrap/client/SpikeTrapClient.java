package com.gitlab.thepeebrain.spiketrap.client;

import com.gitlab.thepeebrain.spiketrap.SpikeTrap;
import com.gitlab.thepeebrain.spiketrap.block.Blocks;
import com.gitlab.thepeebrain.spiketrap.block.enums.TrapCover;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.color.block.BlockColorProvider;
import net.minecraft.client.color.item.ItemColorProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.registry.Registries;

import static net.minecraft.block.Blocks.GRASS_BLOCK;

public class SpikeTrapClient
		implements ClientModInitializer
{
	@Override
	public void onInitializeClient()
	{
		ClientLifecycleEvents.CLIENT_STARTED.register(client -> {
			ColorProviderRegistry<Block, BlockColorProvider> blockColorRegistry = ColorProviderRegistry.BLOCK;
			BlockColorProvider blockColorProvider = blockColorRegistry.get(GRASS_BLOCK);
			blockColorRegistry.register(blockColorProvider, Blocks.FALSE_FLOOR);
			blockColorRegistry.register(blockColorProvider, Blocks.TRAP_FLOOR);

			ColorProviderRegistry<ItemConvertible, ItemColorProvider> itemColorRegistry = ColorProviderRegistry.ITEM;
			ItemColorProvider itemColorProvider = itemColorRegistry.get(GRASS_BLOCK);

			String prefix = TrapCover.GRASS.getRegistryPrefix();
			Item trapFloorItem = Registries.ITEM.get(SpikeTrap.identifier(prefix + "trap_floor"));
			itemColorRegistry.register(itemColorProvider, trapFloorItem);
			Item falseFloorItem = Registries.ITEM.get(SpikeTrap.identifier(prefix + "false_floor"));
			itemColorRegistry.register(itemColorProvider, falseFloorItem);
		});
	}
}

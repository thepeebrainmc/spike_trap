# Changelog
All notable changes to this project will be documented in this file.

## [0.2.0] - 2023-01-02

### Added

- GameRule
  - `spikeTrapBaseDamage`
    - Replaces removed difficulty-based damage

### Changed

- Blocks
  - `trap_floor`
    - Covering can now be done with any player hand
    - Covering and uncovering
      - now requires right-clicking the correct face
      - now requires sneaking
    - Can now open by right-clicking any side but top

### Removed

- GameRules
  - `spikeTrapDifficultyEasy`
  - `spikeTrapDifficultyHard`
  - `spikeTrapDifficultyNormal`

### Fixed

- Crash caused by client event registration in server
- Blocks
  - `trap_floor`
    - item not being consumed when covering with `false_floor`
    - unnecessary block placement when
      - trying to cover with the same cover
      - trying to uncover an uncovered trap
- Game rule category formatting

## [0.1.0] - 2022-12-19

### Added

- Blocks
  - `wooden_spike_trap`
  - `stone_spike_trap`
  - `iron_spike_trap`
  - `golden_spike_trap`
  - `diamond_spike_trap`
  - `netherite_spike_trap`
  - `false_floor`
  - `trap_floor`
- Items
  - `wooden_spike`
  - `stone_spike`
  - `iron_spike`
  - `golden_spike`
  - `diamond_spike`
  - `netherite_spike`
- Game Rules
  - Boolean Rules
      - `spikeTrapDamagesHostileMobs`
      - `spikeTrapDamagesPassiveMobs`
      - `spikeTrapDamagesPlayers`
      - `spikeTrapDestroysItems`
  - Int Rules
    - `falseFloorBreakDelay`
    - `trapFloorDelayClose`
    - `trapFloorDelayOpen`
  - Double Rules
    - `spikeTrapDifficultyEasy`
    - `spikeTrapDifficultyHard`
    - `spikeTrapDifficultyNormal`
    - `spikeTrapMultiplierFall`
    - `spikeTrapMultiplierSneak`
    - `spikeTrapMultiplierWalk`